import {FigureBase} from "./figure-base";

export class FigureQ extends FigureBase {
  constructor() {
    super(0,
      [
        {
          matrix: [
            [0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0],
            [0, 1, 1, 0, 0],
            [0, 1, 1, 0, 0],
            [0, 0, 0, 0, 0],
          ]
        }
      ],
      'blue'
    );
  }
}
