import {FigureBase} from "./figure-base";

export class FigureI extends FigureBase {
  constructor() {
    super(1,
      [
        {
          matrix: [
            [0, 0, 1, 0, 0],
            [0, 0, 1, 0, 0],
            [0, 0, 1, 0, 0],
            [0, 0, 1, 0, 0],
            [0, 0, 0, 0, 0],
          ]
        },
        {
          matrix: [
            [0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0],
            [1, 1, 1, 1, 0],
            [0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0],
          ]
        }
      ],
      'pink'
    );
  }
}
