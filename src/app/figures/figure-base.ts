export class FigureBase {
  private _currentIndex = 0;
  private _position: any[] = [];
  private readonly _color: string = 'grey';

  constructor(index: number, pos: any[], color: string) {
    this._currentIndex = index;
    this._position = pos;
    this._color = color;
  }

  public get color(): string {
    return this._color;
  }

  public getPosition(): number[][] {
    return this._position[this._currentIndex].matrix;
  }

  public getIndexList(): [number, number][] {
    const currentMatrix = this._position[this._currentIndex].matrix;

    const result: [number, number][] = [];

    for (let i = 0; i < currentMatrix.length; i++) {
      for (let j = 0; j < currentMatrix[i].length; j++) {
        if (currentMatrix[i][j] === 1) {
          result.push([i - 2, j - 2]);
        }
      }
    }

    return result;
  }

  public rotate(matrix: number[][], value: number): boolean {
    let index = this._currentIndex;
    if (value > 0) {
      index = index + value === this._position.length ? 0 : index + value;
    } else {
      index = index + value === -1 ? this._position.length - 1 : index + value;
    }
    const result = this.comparisonMatrix(matrix, index);
    if (result) {
      this._currentIndex = index;
    }
    return result;
  }

  public comparisonMatrix(matrix: number[][], index = this._currentIndex): boolean {
    const currentMatrix = this._position[index].matrix;
    for (let i = 0; i < 5; i++) {
      for (let j = 0; j < 5; j++) {
        const value = matrix[i][j] * currentMatrix[i][j];
        if (value === 1) {
          return false;
        }
      }
    }

    return true;
  }
}
