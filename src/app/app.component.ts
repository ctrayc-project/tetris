import {Component, ElementRef, HostListener, OnInit, ViewChild} from '@angular/core';
import {BehaviorSubject, interval, switchMap} from "rxjs";
import {FigureT} from "./figures/figure-t";
import {FigureBase} from "./figures/figure-base";
import {FigureL} from "./figures/figure-l";
import {FigureI} from "./figures/figure-i";
import {FigureQ} from "./figures/figure-q";
import {FigureS} from "./figures/figure-s";
import {FigureZ} from "./figures/figure-z";
import {FigureJ} from "./figures/figure-j";

const colorPalettes: any = {
  yellow: ['#ffe04b', '#e3c73f', '#dabf3b', '#726529'],
  red: ['#fe4c56', '#e24049', '#d93c43', '#72292e'],
  blue: ['#4c50fe', '#4043e2', '#3c3fd9', '#292b72'],
  cyan: ['#4cf6fe', '#40dbe2', '#3cd2d9', '#296f72'],
  pink: ['#fe4cb5', '#e2409f', '#d93c98', '#722954'],
  green: ['#4cfe95', '#40e283', '#3cd97d', '#297247'],
  orange: ['#fe774c', '#e26840', '#d9633c', '#723b29'],
};

export interface IFigure {
  spawn: boolean,
  figure: FigureBase,
  position: {
    x: number,
    y: number,
  }
}

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})


export class AppComponent implements OnInit {
  @ViewChild('myCanvas', {static: false}) canvasRef!: ElementRef;

  public countLine = 0;
  intervalValue$ = new BehaviorSubject<number>(700);

  intervalObservable$: any;
  figure$ = new BehaviorSubject<IFigure | null>(null);

  matrix: { value: number, color: string }[][] = [];

  isBlock = new BehaviorSubject<boolean>(false);
  isPressS = false;

  ctx!: CanvasRenderingContext2D;
  canvas!: HTMLCanvasElement;

  private _nextFigure: FigureBase | null = null;

  @HostListener('window:keyup', ['$event']) clickup(event: any) {
    if (event.code === 'KeyS') {
      this.isPressS = false;
      this.intervalValue$.next(700);
    }
  }

  @HostListener('window:keydown', ['$event']) clickout(event: any) {
    if (event.code === 'KeyD') {
      const figure = this.figure$.value;
      this._clearFigureOnMatrix();
      if (figure && this.checkPosition(figure.position.x + 1, figure.position.y)) {
        figure.position.x += 1;
      }
      this._addFigureOnMatrix();
    }

    if (event.code === 'KeyA') {
      const figure = this.figure$.value;
      this._clearFigureOnMatrix();
      if (figure && this.checkPosition(figure.position.x - 1, figure.position.y)) {
        figure.position.x -= 1;
      }
      this._addFigureOnMatrix();
    }

    if (event.code === 'KeyE') {
      this._rotateFigure(-1);
    }

    if (event.code === 'KeyQ') {
      this._rotateFigure(1);
    }

    if (event.code === 'KeyS') {
      if (!this.isPressS) {
        this.isPressS = true;
        this.intervalValue$.next(25);
      }
    }

  }


  ngOnInit(): void {

    this.intervalObservable$ = this.intervalValue$.pipe(
      switchMap(intervalValue => interval(intervalValue))
    );

    this._createMatrix();
    this._createFigure();


    this.intervalObservable$.subscribe(() => {
      const figure = this.figure$.value;
      if (figure && !this.isBlock.value) {
        if (figure.spawn) {
          figure.spawn = false;
          this._addFigureOnMatrix();
        } else {
          this._clearFigureOnMatrix();
          if (this.checkPosition(figure.position.x, figure.position.y + 1)) {
            figure.position.y += 1;
            this._addFigureOnMatrix();
          } else {
            console.log('end')
            this._addFigureOnMatrix();
            this.isBlock.next(true);
            this._checkMatrix();
            this._createFigure();
            this.isBlock.next(false);
          }
        }
      }
    })
  }

  ngAfterViewInit() {
    this.canvas = this.canvasRef.nativeElement;
    this.ctx = this.canvas.getContext('2d')!;

    if (this.ctx) {
      // Draw a rectangle
      interval(20).subscribe(() => this.draw())
    }
  }

  private draw(): void {
    this.ctx.clearRect(0, 0, this.canvas.width, this.canvas.height);
    this.ctx.fillStyle = '#736031';
    this.ctx.fillRect(0, 0, 2, 724);
    this.ctx.fillRect(364, 0, 2, 724);
    this.ctx.fillRect(0, 722, 364, 2);

    this.ctx.fillRect(400, 40, 2, 160);
    this.ctx.fillRect(560, 40, 2, 160);
    this.ctx.fillRect(400, 200, 162, 2);

    this.ctx.fillStyle = '#ffe04b';
    this.ctx.font = 'Italic 30px Sans-Serif';
    this.ctx.fillText('NEXT', 442, 30)

    this.ctx.fillStyle = '#7e2928';

    if (this._nextFigure) {
      const matrix = this._nextFigure.getPosition();
      matrix.forEach((row, i) => {
        row.forEach((cell, j) => {
          if (cell === 1) {
            const color: any = colorPalettes[this._nextFigure!.color];
            this.ctx.fillStyle = color[0];
            this.ctx.fillRect(j * 36 + 400, i * 36 + 1, 34, 34);
            this.ctx.fillStyle = color[1];
            this.ctx.fillRect(j * 36 + 400, i * 36 + 3, 32, 30);

            let radgrad = this.ctx.createRadialGradient(
              j * 36 + 420,
              i * 36 + 3,
              10,
              j * 36 + 420,
              i * 36 + 20,
              30);
            radgrad.addColorStop(0, color[2]);
            radgrad.addColorStop(0.9, color[3]);
            radgrad.addColorStop(1, 'rgba(1,159,98,0)');

            this.ctx.fillStyle = radgrad;
            this.ctx.fillRect(j * 36 + 4, i * 36 + 4, 32, 31);
          }
        })
      })
    }

    this.matrix.forEach((row, i) => {
      row.forEach((cell, j) => {
        if (cell.value === 1) {
          const color: any = colorPalettes[cell.color];
          this.ctx.fillStyle = color[0];
          this.ctx.fillRect(j * 36 + 4, i * 36 + 1, 34, 34);
          this.ctx.fillStyle = color[1];
          this.ctx.fillRect(j * 36 + 4, i * 36 + 3, 32, 30);

          let radgrad = this.ctx.createRadialGradient(
            j * 36 + 20,
            i * 36 + 3,
            10,
            j * 36 + 20,
            i * 36 + 20,
            30);
          radgrad.addColorStop(0, color[2]);
          radgrad.addColorStop(0.9, color[3]);
          radgrad.addColorStop(1, 'rgba(1,159,98,0)');

          this.ctx.fillStyle = radgrad;
          this.ctx.fillRect(j * 36 + 4, i * 36 + 4, 32, 31);
        }
      })
    })

    this.ctx.restore()
  }

  private _rotateFigure(value: number): void {
    const {x, y} = this.figure$.value!.position;
    this._clearFigureOnMatrix();
    const isRotate = this.figure$.value?.figure.rotate(this.getMatrixSlice5x5(x, y), value);
    this._addFigureOnMatrix();
  }

  private _clearFigureOnMatrix(): void {
    const {x, y} = this.figure$.value!.position;
    const indexList = this.figure$.value?.figure.getIndexList();

    indexList?.forEach((values) => {
      this.matrix[y + values[0]][x + values[1]].value = 0;
    })
  }

  private _addFigureOnMatrix(): void {
    const {x, y} = this.figure$.value!.position;
    const indexList = this.figure$.value?.figure.getIndexList();

    indexList?.forEach((values) => {
      this.matrix[y + values[0]][x + values[1]] = {
        value: 1,
        color: this.figure$.value?.figure.color!,
      };
    })
  }

  private checkPosition(x: number, y: number): boolean {
    const matrix: number[][] = this.getMatrixSlice5x5(x, y);
    console.log(matrix)
    return this.figure$.value?.figure.comparisonMatrix(matrix) ?? false;
  }

  private getMatrixSlice5x5(x: number, y: number): number[][] {
    const matrix: number[][] = [];
    for (let i = 2; i >= -2; i--) {
      const row = [];
      for (let j = -2; j <= 2; j++) {
        if (y - i < 0 || y - i > 19 || x - j < 0 || x - j > 9) {
          row.push(1);
        } else {
          row.push(this.matrix[y - i][x - j].value);
        }

      }
      matrix.push(row.reverse());
    }

    return matrix;
  }


  private _checkMatrix(): void {

    this.matrix.forEach(((row, index) => {
      let fullRow = !row.find(cell => cell.value === 0);
      if (fullRow) {
        this._deleteRow(index);
      }
    }))
  }

  private _deleteRow(index: number): void {
    this.countLine++;
    this.matrix[index].forEach((cell => cell.value = 0))
    for (let i = index; i > 0; i--) {
      for (let j = 0; j < 10; j++) {
        this.matrix[i][j] = JSON.parse(JSON.stringify(this.matrix[i - 1][j]));
      }
    }
  }

  private _getRandomFigure(): FigureBase {
    const figures = [new FigureT(), new FigureL(), new FigureI(), new FigureJ(), new FigureQ(), new FigureS(), new FigureZ(),];
    const randomIndex = Math.floor(Math.random() * figures.length);
    return figures[randomIndex];
  }

  private _createFigure(): void {

    const figure = {
      spawn: true,
      figure: this._nextFigure ?? this._getRandomFigure(),
      position: {
        x: 5,
        y: 0,
      }
    };

    this._nextFigure = this._getRandomFigure();

    this.figure$.next(figure);
  }

  private _createMatrix(): void {
    const width = 10;
    const height = 20;

    for (let i = 0; i < height; i++) {
      const row = [];
      for (let j = 0; j < width; j++) {
        row.push({value: 0, color: 'none'});
      }
      this.matrix.push(row);
    }

  }

}
